docker build -t dockeragent:latest .

docker run -e AZP_URL=https://dev.azure.com/<organisation> -e AZP_TOKEN=<token> -e AZP_AGENT_NAME=mydockeragent dockeragent:latest
